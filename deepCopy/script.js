const deepClone = (obj) => {
    const cycleLink = new WeakMap();

    const clone = (acc) => {
        if (typeof acc !== 'object' || acc === null) {
            return acc;
        }
        if (cycleLink.has(acc)) {
            return cycleLink.get(acc);
        }

        const cloneObj = Array.isArray(acc) ? [] : {};

        cycleLink.set(acc, cloneObj);

        if (Object.getPrototypeOf(acc) !== null) {
            Object.setPrototypeOf(cloneObj, Object.getPrototypeOf(acc));
        }

        for (const key in acc) {
            if (Object.prototype.hasOwnProperty.call(acc, key)) {
                cloneObj[key] = clone(acc[key]);
            }
        }
        return cloneObj;
    };
    return clone(obj);
};


//----------------------------------------------
///// Тест 1
const orig = {
    a: 'dasda',
    b: {
        a: 'cxc',
        c: {
            cl: 'dsada',
            func: () => console.log('Hello world')
        }
    }
}
const clone = (deepClone(orig));
const clone2 = (deepClone(orig));
clone.b.a = '31231';
clone2.b.c = {
    a: 'a',
    b: 'b'
};
console.log('----Тест1----');
console.log(orig);
console.log(clone);
console.log(clone2);
console.log('---------------------------------');




//----------------------------------------------
// Тест2 Прототип
const protoObj = { x: 10, y: 20 };
const objProto = Object.create(protoObj);

const clonedObjProto = deepClone(protoObj);

console.log(clonedObjProto instanceof protoObj.constructor);
console.log('---------------------------------');



//----------------------------------------------
/// Тест 3
const objA = {
    d: 'hello D'
};
const objB = {
    a: 'hello A'
};
objA.refToB = objB.a;
objB.refToA = objA.d;

const clonedObj = deepClone(objA);

console.log('----Тест2----');
console.log(clonedObj);
console.log('---------------------------------');




//----------------------------------------------
// Тест 4
let obj = {};
obj.a = obj;

const clonedObj2 = deepClone(obj);
clonedObj2.a = 'non ref'

console.log('----Тест3----');
console.log(obj);
console.log(clonedObj2);
console.log('---------------------------------');




//----------------------------------------------
//Тест 5
const home = {
    owner: {
        name: 'Misha',
        homes: null
    }
}
home.owner.homes = home;

const clonedObj3 = deepClone(home);
clonedObj3.owner.name = 'Dima';
clonedObj3.owner.homes.owner.homes.owner = 'Run';


console.log('----Тест4----');
console.log(home);
console.log(clonedObj3);
console.log('---------------------------------');




