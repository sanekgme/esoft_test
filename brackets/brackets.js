function correctBrackets(bracketsString) {
    const brackets = {
        ']': '[',
        '}': '{',
        ')': '(',
    };

    let stack = [];

    for (let i = 0; i < bracketsString.length; i++) {
        let curr = bracketsString[i];
        if ([')', '}', ']'].indexOf(curr) == -1) {
            stack.push(curr)
        } else {
            if (brackets[curr] !== stack.pop()) {
                return false;
            }
        }
    }
    return stack.length === 0;
}

console.log('()', correctBrackets('()'));
console.log('()[]{}', correctBrackets('()[]{}'));
console.log('(]', correctBrackets('(]'));
console.log('([)]', correctBrackets('([)]'));
console.log('{[]}', correctBrackets('{[]}'));


